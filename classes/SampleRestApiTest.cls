@IsTest
private class SampleRestApiTest {

    @TestSetup
    static void testDataSetup(){
        Account testAccount = new Account(Name = 'The Beatles');
        insert testAccount;
        
        Contact john = new Contact(FirstName = 'John', LastName = 'Lennon', Email = 'john@thebeatles.com', AccountId = testAccount.Id);
        Contact paul = new Contact(FirstName = 'Paul', LastName = 'McCartney', Email = 'paul@thebeatles.com', AccountId = testAccount.Id);
        Contact george = new Contact(FirstName = 'George', LastName = 'Harrison', Email = 'george@thebeatles.com', AccountId = testAccount.Id);
        Contact ringo = new Contact(FirstName = 'Ringo', LastName = 'Starr', Email = 'ringo@thebeatles.com', AccountId = testAccount.Id);
        insert new List<Contact>{john, paul, george, ringo};
    }
    
    @IsTest
    static void testGetRequestExactMatch(){
        // exactly matches one of the test contacts
        prepGetRequest('Paul', 'McCartney', 'paul@thebeatles.com');
        
        Test.startTest();
        SampleRestApi.getContactsWithName();
        Test.stopTest();
        
        String responseBodyString = RestContext.response.responseBody.toString();
        List<Object> results = (List<Object>) JSON.deserializeUntyped(responseBodyString);
        
        // 1 match should be returned in succesful API response (200 status code)
        System.assertEquals(1, results.size());
        System.assertEquals(200, RestContext.response.statusCode);
    }
    
    @IsTest
    static void testGetRequestPartialMatch(){
        // matches firstname and email one of the test contacts (but not last name)
        prepGetRequest('John', 'Epstein', 'john@thebeatles.com');
        
        Test.startTest();
        SampleRestApi.getContactsWithName();
        Test.stopTest();
        
        String responseBodyString = RestContext.response.responseBody.toString();
        List<Object> results = (List<Object>) JSON.deserializeUntyped(responseBodyString);
        
        // 1 match should still be returned since 2 of 3 properties match
        System.assertEquals(1, results.size());
        System.assertEquals(200, RestContext.response.statusCode);
    }
    
    @IsTest
    static void testGetRequestNoMatch(){
        prepGetRequest('Pete', 'Best', 'pete@thebeatles.com');
        
        Test.startTest();
        SampleRestApi.getContactsWithName();
        Test.stopTest();
        
        String responseBodyString = RestContext.response.responseBody.toString();
        List<Object> results = (List<Object>) JSON.deserializeUntyped(responseBodyString);
        
        // an empty array should be returned since no matching contact exists
        System.assertEquals(0, results.size());
        System.assertEquals(200, RestContext.response.statusCode); 
    }
    
    @IsTest
    static void testGetRequestInvalidRequest(){
        // this is an invalid request since the firstName is an empty string
        prepGetRequest('', 'Harrison', 'george@thebeatles.com');
        
        Test.startTest();
        SampleRestApi.getContactsWithName();
        Test.stopTest();
        
        // bad request error (status of 400) should be returned since no first name was provided
        System.assertEquals(400, RestContext.response.statusCode); 
    }
    
    @IsTest
    static void testGetRequestException(){
        // valid request matching an existing contact
        prepGetRequest('Ringo', 'Starr', 'ringo@thebeatles.com');
        
        Test.startTest();
        SampleRestApiService.throwError = true; // simulates Salesforce exception
        SampleRestApi.getContactsWithName();
        Test.stopTest();

        // server error (status of 500) should be returned since a Salesforce exception occurred
        System.assertEquals(500, RestContext.response.statusCode); 
    }
        
    private static void prepGetRequest(String firstName, String lastName, String email){
        RestRequest request = new RestRequest();
        request.httpMethod = 'GET';
        request.addParameter('firstName', firstName);
        request.addParameter('lastName', lastName);
        request.addParameter('email', email);
        
        RestContext.request = request;
        RestContext.response = new RestResponse();
    }   
}