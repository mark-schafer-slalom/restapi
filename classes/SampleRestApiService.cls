public without sharing class SampleRestApiService {

    private GetRequestWrapper getRequest;
    
    @TestVisible
    private static Boolean throwError;

    public SampleRestApiService() {
        this.getRequest = new GetRequestWrapper();
    }

    public void getMachingContacts(){
        // Return 400 error if any required params are missing from request
        if(!getRequest.hasRequiredParams()){
            ApiError error = new ApiError('BAD_REQUEST', 'The following parameters are required: firstName, lastName and email');
            error.returnErrorResponse(400);
            return;
        }

        try{
            // enables simulating Salesforce errors in test class
            if (Test.isRunningTest() && throwError == true) {
                this.throwUnitTestError();    
            }
            
            // response body will be JSON array of GetResponse objects representing details of matching contacts
            List<GetResponse> responseList = new List<GetResponse>(); 
            for(Contact matchContact : this.queryMatchingContacts()) {
                responseList.add(new GetResponse(matchContact));
            }
            this.setSuccessResponse(responseList);
        }
        catch (Exception e){
            ApiError error = new ApiError('INTERNAL_SERVER_ERROR', e.getMessage());
            error.returnErrorResponse(500);
        }
    }

    private List<Contact> queryMatchingContacts(){
        List<Contact> matchingContacts = [
            SELECT Id, FirstName, LastName, Email, AccountId
            FROM Contact
            WHERE (
                (FirstName = :this.getRequest.firstName AND LastName = :this.getRequest.lastName)
                OR (FirstName = :this.getRequest.firstName AND Email = :this.getRequest.email)
                OR (LastName = :this.getRequest.lastName AND Email = :this.getRequest.email)
            )
        ];
        return matchingContacts;
    }
    
    // serialize the reponse to JSON string and include in response body as a blob
    private void setSuccessResponse(Object responseObject){
        String serializedResponse = JSON.serialize(responseObject);
        RestContext.response.responseBody = Blob.valueOf(serializedResponse);
        RestContext.response.statusCode = 200;
    }
    
    // used only in tests to simulate server-side Salesforce errors 
    private void throwUnitTestError(){
        CalloutException e = new CalloutException();
        e.setMessage('Unit Test Error');
        throw e;
    }

    // wrapper class to extract, store and validate URL param values passed in with request
    public class GetRequestWrapper{
        public String firstName;
        public String lastName;
        public String email;

        public GetRequestWrapper(){
            this.firstName = RestContext.request.params.get('firstName');
            this.lastName = RestContext.request.params.get('lastName');
            this.email = RestContext.request.params.get('email');
        }

        public Boolean hasRequiredParams(){
            if(String.isBlank(this.firstName) || String.isBlank(this.lastName) || String.isBlank(this.email)){
                return false;
            }
            return true;
        }
    }

    // wrapper class to contain each contact in the response, giving easy control of the structure of the JSON response, including property names
    public class GetResponse{
        String contactId;
        String accountId;
        String firstName;
        String lastName;
        String email;

        public GetResponse(Contact contactMatch){
            this.contactId = contactMatch.Id;
            this.accountId = contactMatch.AccountId;
            this.firstName = contactMatch.FirstName;
            this.lastName = contactMatch.LastName;
            this.email = contactMatch.Email;
        }
    }

    // wrapper class to handle returning an error code and message in the Rest response
    public class ApiError {
        public String errorCode;
        public String message;

        public ApiError(String errorCode, String message){
            this.errorCode = errorCode; 
            this.message = message;
        }

        public void returnErrorResponse(Integer statusCode){
            RestContext.response.statusCode = statusCode;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(this));
        }
    }

}