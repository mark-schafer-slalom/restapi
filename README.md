# README #

An example implementation of exposing Apex code as a REST web service.

In this example, the API receives a first name, last name, and email address as URL paremeters in the GET request. 

The code queries existing Contacts for records that exactly match at least 2 of the parameters and returns details about those records as an array in the JSON string in the response body.

See Salesforce documentation for more details about exposting Apex as REST web services:
https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_rest.htm


### Package Contents ###
Three Apex classes:

* **SampleRestApiv1** - exposes a GET method as a global method available as a REST endpoint (see @RestResource and @HttpGet annotations)
* **SampleRestApiService** - the actual implementation of the API, encapsulating data and functionality in a separate class
* **SampleRestApiTest** - a test class covering the example functionality and demonstrating how an Apex REST web service can be tested

### Implementation Overview ###
Noteable aspects of implementation in SampleRestApiService

* REST request and and response are encapsulated in RestRequest and RestResponse classes, accessible via RestContext
* The parameters supplied in the request are extracted from RestContext.request.params and put in an inner wrapper class for easy accessibility in the code
* A method on the inner class is called to check if the supplied parameter values are valid or not
    * If not, details of an error are stored in an inner class and a client request error is returned
    * This does not throw a Salesforce exception. Instead, it returns an HTTP response with a 400 status and an error message to the client
* A query is performed for Contact records that match 2 of the 3 request param values firstName, lastName and email
* Results are stored in an array of inner wrapper class instances
    * The wrapper class can be easily stringified into valid JSON with whatever structure and property names are desired
    * If an SObject record is stringified to JSON directly, a more complicated JSON object string defined by Salesforce is returned instead, so that's not recommended in most cases
* A try-catch block is used to handle internal Salesforce exception
    * The same inner class as the 400 error is used to return an error response, but this time with a 500 status and details of the Salesforce exception
    * The TestVisible variable throwError is used to simulate Salesforce exceptions in the test class for better code coverage
* If no 400 or 500 error is encountered, the client receives an HTTP response with a status of 200 and an array of matches in the body
    * If no matches are found, a 200 status is still returned, but the JSON array in the response body will be empty

### How do I use and test the API? ###

* A Connected App must be configured so external clients can access the Salesforce REST API
* A valid/unexpired OAuth token must be obtained from the Salesforce instance and then passed with the request
* A client such as Postman can be used to access and test the API


### Getting an API Token ###

Send a PUT Request to one of the below endpoints depending on the type of envrionment
Sandboxes: https://test.salesforce.com/services/oauth2/token
Production/Dev Orgs: https://login.salesforce.com/services/oauth2/token

URL Parameters

| Param | Value |
|--------|---------|
| grant_type | password|
| username | (Admin user password) |
| password | (Admin user password w/ security token) | 
| client_id | (Client ID from Connected App) |
| client_secret| (Client Secret from Connected App) |


### Sending a Request ###
Send a GET request to the endpoint below
https://(MY DOMAIN).my.salesforce.com/services/apexrest/claimAccount


Request Headers

| Header | Value |
|--------|---------|
| Authorization | Bearer (OAUTH TOKEN) |


URL Parameters

| Param | Value |
|--------|---------|
| email | (an email address) |
| firstName | (first name value) |
| lastName | (last name value) |